<?php
require __DIR__. '/__connect_db_jo.php';



$sql = "SELECT * FROM series";

$result = $mysqli->query($sql);

$data = array();

while($row = $result->fetch_assoc()) {
    if($row['parent_id']==0){
        $data[ $row['sid'] ] = array(
            'title' => $row['series_name'],
            'sub' => array(),

        );
    }
}
$result->data_seek(0);
while($row = $result->fetch_assoc()) {
    if($row['parent_id']!=0){

        $data[ $row['parent_id'] ]['sub'][$row['sid']] = $row['series_name'];

    }
}


echo json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);




