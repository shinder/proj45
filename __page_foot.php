<script src="lib/jquery-1.12.0.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="lib/underscore.js"></script>
<script>
    var countTotal = function(obj){
        var badge = $('.badge');
        badge.hide();
        var totalItems = 0;
        if(!obj) {
            $.get('add_to_cart.php', function(data){

                for(var s in data) {
                    totalItems += data[s];
                }
                badge.text(totalItems);
            }, 'json');
        } else {
            data = obj;
            for(var s in data) {
                totalItems += data[s];
            }
            badge.text(totalItems);
        }
        badge.fadeIn();
    };
    countTotal();
</script>
</body>
</html>