<?php
require __DIR__. '/__connect_db.php';
$page = 'product_list';
$title = '產品列表';
$maxRows = 4;

// --- 分類
if(isset($_GET['cate'])){
    $_SESSION['cate'] = intval($_GET['cate']);
    $_SESSION['pageNum'] = 1;
}
if(!isset($_SESSION['cate'])){
    $_SESSION['cate'] = 0;
    $_SESSION['pageNum'] = 1;
}
$cate_id = $_SESSION['cate'];

$cate_result = $mysqli->query("SELECT * FROM categories WHERE parent_sid=0");

$where = ' WHERE 1 ';
if($cate_id!=0){
    $where .= " AND category_sid=$cate_id";
}

// --- 搜尋
if(isset($_GET['search'])){
    $_SESSION['search'] = $_GET['search'];
    $_SESSION['pageNum'] = 1;
}
if(!isset($_SESSION['search'])){
    $_SESSION['search'] = '';
    $_SESSION['pageNum'] = 1;
}
$search = $_SESSION['search'];

if(! empty($search)){
    $search = $mysqli->escape_string($search);
    $where .= " AND (`author` LIKE '%{$search}%' OR `bookname` LIKE '%{$search}%'   )";
}





$result = $mysqli->query("SELECT 1 FROM products $where");
$totalRows = $result->num_rows;
$totalPages = ceil($totalRows/$maxRows);


if(isset($_GET['pageNum'])){
    $_SESSION['pageNum'] = intval($_GET['pageNum']);
}
if(!isset($_SESSION['pageNum'])){
    $_SESSION['pageNum'] = 1;
}
$pageNum = $_SESSION['pageNum'];

$sql = sprintf("SELECT * FROM products %s LIMIT %s, %s", $where, ($pageNum-1)*$maxRows, $maxRows);
// echo $sql;
$result = $mysqli->query($sql);


?>
<?php include __DIR__. '/__page_head.php'; ?>
<?php include __DIR__. '/__page_navbar.php'; ?>
<div class="container">
    <div class="col-lg-3">
        <div class="col-lg-12">
            <div class="btn-group-vertical col-lg-12">
                <a href="?cate=0" type="button"
                   class="btn btn-default <?= $cate_id==0 ? 'active' : '' ?>">全部商品</a>
                <?php while($row=$cate_result->fetch_assoc()): ?>
                <a href="?cate=<?= $row['sid'] ?>" type="button"
                   class="btn btn-default <?= $cate_id==$row['sid'] ? 'active' : '' ?>">
                    <?= $row['name'] ?></a>
                <?php endwhile; ?>
<!--                <button type="button"  class="btn btn-info">2</button>-->
            </div>
        </div>
    </div>

    <div class="col-lg-9">
        <div class="col-lg-12">
            <div class="col-lg-6">
                <ul class="pagination">
                    <li><a href="?pageNum=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                    <?php for($i=$pageNum-3; $i<=$pageNum+3; $i++):
                        if($i>=1 and $i<=$totalPages):
                            $active = $i==$pageNum ? 'active' : '';
                            printf('<li class="%s"><a href="?pageNum=%s">%s</a></li>', $active, $i, $i);

                        endif;
                    endfor; ?>
                    <li><a href="?pageNum=<?= $totalPages ?>" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                </ul>

            </div>
            <div class="col-lg-6">
                <form class="form-inline" method="get">
                    <div class="form-group">
                        <input type="text" class="form-control" value="<?= $_SESSION['search'] ?>"
                               id="search" name="search" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
            </div>
        </div>

        <?php while($row=$result->fetch_assoc()): ?>
        <div class="col-lg-3 col-sm-4 col-xs-6">
            <div class="thumbnail" style="height:280px; margin:10px 0;">
                <a class="single_product" href="single_product.php?sid=23" target="_blank">
                    <img src="imgs/small/<?= $row['book_id'] ?>.jpg" style="width: 100px; height: 135px;">
                </a>
                <div class="caption">
                    <h5><?= $row['bookname'] ?></h5>
                    <h5><?= $row['author'] ?></h5>
                    <p>
                        <span class="glyphicon glyphicon-search"></span>
                        <span class="label label-info">$ <?= $row['price'] ?></span>
                        <select name="qn" class="qn">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>
                        <button class="btn btn-warning btn-sm buy_btn" data-sid="23">買</button>
                    </p>
                </div>
            </div>
        </div>
        <?php endwhile; ?>


    </div>

</div>

<?php include __DIR__. '/__page_foot.php'; ?>
<script></script>
