SELECT * FROM `products` JOIN `categories`;

SELECT * FROM `products` JOIN `categories` ON `products`.category_sid=categories.sid;

SELECT * FROM `products` AS p JOIN `categories` AS c ON p.category_sid=c.sid;

SELECT p.*, c.name AS cate_name FROM `products` AS p JOIN `categories` AS c ON p.category_sid=c.sid;

SELECT p.*, c.name cate_name FROM `products` p JOIN `categories` c ON p.category_sid=c.sid;

SELECT p.*, c.name cate_name FROM `products` p LEFT JOIN `categories` c ON p.category_sid=c.sid;

ALTER TABLE `categories` ADD `parent_sid` INT NOT NULL DEFAULT '0' AFTER `name`;

SELECT *  FROM `categories` c1 LEFT JOIN `categories` c2 ON c1.parent_sid=c2.sid;

SELECT c1.*, c2.name parent_name  FROM `categories` c1 LEFT JOIN `categories` c2 ON c1.parent_sid=c2.sid;

SELECT p.*, c1.name cate_name, c2.name pa_cate_anme FROM products p
  JOIN categories c1 ON p.category_sid=c1.sid
  JOIN categories c2 ON c1.parent_sid=c2.sid;

SELECT p.*, c1.name cate_name, c2.name pa_cate_anme FROM products p
  LEFT JOIN categories c1 ON p.category_sid=c1.sid
  LEFT JOIN categories c2 ON c1.parent_sid=c2.sid;


-- tales
SELECT p.*, c.name `分類`, s.name  `系列` FROM `product` p JOIN `categories` c ON p.`categories_sid`=c.sid
JOIN `porcelain_series` s ON p.`series_sid`=s.sid;

SELECT p.*, c.name `分類`, s.name  `系列` FROM `product` p JOIN `categories` c ON p.`categories_sid`=c.sid
JOIN `porcelain_series` s ON p.`series_sid`=s.sid WHERE p.name_chinese LIKE '%茶海%';

SELECT p.*, c.name `分類`, s.name  `系列` FROM `product` p JOIN `categories` c ON p.`categories_sid`=c.sid
JOIN `porcelain_series` s ON p.`series_sid`=s.sid
 WHERE p.name_chinese LIKE '%茶壺%' OR s.name LIKE '%茶壺%';
