<?php
    require __DIR__. '/__connect_db.php';
    $page = 'member_data';

    if(! isset($_SESSION['user'])){
        header('Location: ./');
        exit;
    }

    $sid = $_SESSION['user']['sid'];

    if(isset($_POST['password'])) {
        $password = sha1($_POST['password']);

        $result = $mysqli->query("SELECT * FROM `members` 
WHERE `sid`=$sid AND `password`='$password'");

        if($result->num_rows) {
            // $row = $result->fetch_assoc();
            if(empty($_POST['new_password'])) {
                $sql = "UPDATE `members` SET `nickname`=?,`mobile`=?,`address`=? WHERE sid=$sid";
                $stmt = $mysqli->prepare($sql);
                $stmt->bind_param("sss",
                    $_POST['nickname'],
                    $_POST['mobile'],
                    $_POST['address']
                    );

            } else {
                $sql = "UPDATE `members` SET `password`=?, `nickname`=?,`mobile`=?,`address`=? WHERE sid=$sid";
                $stmt = $mysqli->prepare($sql);
                $stmt->bind_param("ssss",
                    sha1($_POST['new_password']),
                    $_POST['nickname'],
                    $_POST['mobile'],
                    $_POST['address']
                );
            }

            $stmt->execute();
            $stmt->close();

            $flash = array(
                'class' => 'success',
                'msg' => '修改完成',
            );
            $_SESSION['user']['nickname'] = $_POST['nickname'];
            $_SESSION['user']['mobile'] = $_POST['mobile'];
            $_SESSION['user']['address'] = $_POST['address'];

        } else {
            $flash = array(
                'class' => 'danger',
                'msg' => '帳號或密碼錯誤',
            );

        }

    }


?>
<?php include __DIR__. '/__page_head.php'; ?>

<?php include __DIR__. '/__page_navbar.php'; ?>

<div class="container">

    <div class="col-lg-6">
        <?php if(isset($flash)): ?>
            <div class="alert alert-<?= $flash['class'] ?>" role="alert"><?= $flash['msg'] ?></div>
        <?php endif; ?>
        <div class="panel panel-default">
            <div class="panel-heading">會員資料修改</div>
            <div class="panel-body">

                <form method="post">
                    <div class="form-group">
                        <label for="email_id">* Email 帳號</label>
                        <input type="text" class="form-control" id="email_id"  name="email_id"
                        value="<?= $_SESSION['user']['email_id'] ?>" disabled="disabled">
                    </div>
                    <div class="form-group">
                        <label for="password">* 密碼</label>
                        <input type="password" class="form-control" id="password"  name="password">
                    </div>

                    <div class="form-group">
                        <label for="new_password">新密碼 (空白表示不變更)</label>
                        <input type="password" class="form-control" id="new_password"  name="new_password">
                    </div>

                    <div class="form-group">
                        <label for="nickname">* 暱稱</label>
                        <input type="text" class="form-control" id="nickname"  name="nickname"
                               value="<?= $_SESSION['user']['nickname'] ?>">
                    </div>

                    <div class="form-group">
                        <label for="mobile">手機</label>
                        <input type="text" class="form-control" id="mobile"  name="mobile"
                               value="<?= $_SESSION['user']['mobile'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="address">地址</label>
                        <textarea class="form-control"  name="address" id="address" cols="30" rows="10"><?= $_SESSION['user']['address'] ?></textarea>
                    </div>

                    <button type="submit" class="btn btn-default">修改</button>

                </form>

            </div>
        </div>
    </div>


</div>

<?php include __DIR__. '/__page_foot.php'; ?>
<script></script>
