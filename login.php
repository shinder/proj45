<?php
    require __DIR__. '/__connect_db.php';
    $page = 'login';

    $email_id = '';


    if(isset($_POST['email_id'])) {
        $email_id = $mysqli->escape_string( $_POST['email_id'] );
        $password = sha1( $_POST['password'] );


        $result = $mysqli->query("SELECT * FROM `members` 
WHERE `email_id`='$email_id' AND `password`='$password'");

        if($result->num_rows) {
            $row = $result->fetch_assoc();
            $_SESSION['user'] = $row;
            if(isset($_SESSION['goback'])){
                header('Location: '. $_SESSION['goback']);
            }else{
                header('Location: ./');
            }

            exit;
        } else {
            $flash = "帳號或密碼錯誤";
        }
    } else {
        $_SESSION['goback'] = $_SERVER['HTTP_REFERER'];
    }


?>
<?php include __DIR__. '/__page_head.php'; ?>

<?php include __DIR__. '/__page_navbar.php'; ?>

<div class="container">
    <div class="col-lg-6">
        <?php if(isset($flash)): ?>
            <div class="alert alert-danger" role="alert"><?= $flash ?></div>
        <?php endif; ?>
        <div class="panel panel-default">
            <div class="panel-heading">會員登入</div>
            <div class="panel-body">

                <form method="post">
                    <div class="form-group">
                        <label for="email_id">* Email 帳號</label>
                        <input type="text" class="form-control" id="email_id"  name="email_id"
                        value="<?= $email_id ?>">
                    </div>

                    <div class="form-group">
                        <label for="password">* 密碼</label>
                        <input type="password" class="form-control" id="password"  name="password">
                    </div>


                    <button type="submit" class="btn btn-default">登入</button>

                </form>

            </div>
        </div>
    </div>

</div>

<?php include __DIR__. '/__page_foot.php'; ?>
<script></script>
