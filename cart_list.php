<?php
require __DIR__ . '/__connect_db.php';
require __DIR__ . '/__tools.php';
$page = 'cart_list';
$title = '購物車';

if(! empty($_SESSION['cart'])){
    $keys = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM products WHERE sid IN (%s)", implode(',', $keys));
    $result = $mysqli->query($sql);

    $p_data = array();

    while($row=$result->fetch_assoc()) {
        $row['qn'] = $_SESSION['cart'][$row['sid']];
        $p_data[ $row['sid'] ] = $row;

        //$p_data[ $row['sid'] ] = $row;
        //$p_data[ $row['sid'] ]['qn'] = $_SESSION['cart'][$row['sid']];
    }

    //echo json_encode($p_data, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
}


?>
<?php include __DIR__ . '/__page_head.php'; ?>
<?php include __DIR__ . '/__page_navbar.php'; ?>
<div class="container">
    <div class="col-lg-12">
        <?php if(! empty($_SESSION['cart'])): ?>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>取消</th>
                <th>書名</th>
                <th>作者</th>
                <th>封面</th>
                <th>單價</th>
                <th>數量</th>
                <th>小計</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($keys as $v): ?>
            <tr data-sid="<?= $v ?>">
                <th scope="row" style="font-size: 20px">
                    <span class="glyphicon glyphicon-remove-sign remove_item" aria-hidden="true"></span>
                </th>
                <td><?=  $p_data[$v]['bookname'] ?></td>
                <td><?=  $p_data[$v]['author'] ?></td>
                <td><img src="imgs/small/<?=  $p_data[$v]['book_id'] ?>.jpg" alt=""></td>
                <td data-price=""><?=  $p_data[$v]['price'] ?></td>
                <td>
                    <select class="form-control qn" data-qn="<?=  $p_data[$v]['qn'] ?>">
                        <?php for($i=1; $i<=20;$i++):
                            //$se = $i==$p_data[$v]['qn'];
                            ?>
                            <?php /*
                        <option <?= $se ? 'selected ':'' ?>value="<?= $i ?>"><?= $i ?></option>
                            */ ?>
                        <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor; ?>
                    </select>



                </td>
                <td class="subTotal"><?=  $p_data[$v]['price']*$p_data[$v]['qn'] ?></td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php endif; ?>

    </div>
    <div class="col-lg-6">
        <div class="alert alert-success" role="alert">總計: <span id="totalAmount"></span></div>
    </div>
    <div class="col-lg-6">
        <?php if(isset($_SESSION['user'])): ?>
            <a class="btn btn-info" href="pay_it.php">結帳</a>
        <?php else: ?>
            <div class="alert alert-danger" role="alert">請先登入會員再結帳</div>
        <?php endif; ?>

    </div>
</div>

<?php include __DIR__ . '/__page_foot.php'; ?>
<script>
    var select_qn = $('select.qn');
    var calTotalAmount = function(){
        var t = 0;
        $('.subTotal').each(function(){
            t+= parseInt( $(this).text() );
        });

        $('#totalAmount').text(t);
    };

    $('.remove_item').click(function(){
        var tr = $(this).closest('tr');
        var sid = tr.data('sid');

        $.get('add_to_cart.php', {sid:sid}, function(data){
            countTotal(data);
            tr.remove();
            calTotalAmount();
        }, 'json');
    });

    select_qn.each(function(){
        var qn = $(this).data('qn');
        $(this).val(qn);
    });

    select_qn.change(function(event){
        var sid = $(this).closest('tr').data('sid');
        var qn = $(this).val();
        var me = $(this);
        console.log(1, this);
        //console.log({sid:sid, qn:qn});
        $.get('add_to_cart.php', {sid:sid, qn:qn}, function(data){
            console.log(2, this);
            countTotal(data);
            var price = me.parent().prev().text();
            var subt_td = me.parent().next();

            subt_td.text(qn*price);
            calTotalAmount();
        }, 'json');
    });

    calTotalAmount();
</script>
