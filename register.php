<?php
    require __DIR__. '/__connect_db.php';
    $page = 'register';
    $email_id = '';
    $nickname = '';

    if(isset($_POST['email_id'])) {
        $email_id = $_POST['email_id'];
        $nickname = $_POST['nickname'];
        $cert = md5($_POST['email_id']. uniqid());

        $stmt = $mysqli->prepare("INSERT INTO `members`(`email_id`, `password`, `nickname`, `mobile`, `address`, `created_at`, `certification`)
 VALUES
 (?,?,?,?,?, NOW(), ?)
 ");

        $stmt->bind_param("ssssss",
            $email_id,
            sha1($_POST['password']),
            $nickname,
            $_POST['mobile'],
            $_POST['address'],
            $cert

            );

        $stmt->execute();
        $stmt->close();

        require __DIR__. '/__mail_settings.php';

        $mail->addAddress($email_id, $nickname);
        $mail->Subject = 'OOO activation code';
        $mail->Body    = "<a href='http://192.168.22.117/proj45/activate.php?code=$cert'>http://192.168.22.117/proj45/activate.php?code=$cert</a>";
        $mail->AltBody = "http://192.168.22.117/proj45/activate.php?code=$cert";
        if(!$mail->send()) {
//            echo 'Message could not be sent.';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
        $flash = "感謝您的註冊, 請收信啟用帳號";


    }


?>
<?php include __DIR__. '/__page_head.php'; ?>

<?php include __DIR__. '/__page_navbar.php'; ?>

<div class="container">

    <div class="col-lg-6">
        <?php if(isset($flash)): ?>
            <div class="alert alert-success" role="alert"><?= $flash ?></div>
        <?php endif; ?>
        <div class="panel panel-default">
            <div class="panel-heading">會員註冊</div>
            <div class="panel-body">

                <form name="form1" method="post" onsubmit="return formCheck()">
                    <div class="form-group">
                        <label for="email_id">* Email 帳號</label>
                        <input type="text" class="form-control" id="email_id"  name="email_id"
                        value="<?= $email_id ?>">
                        <div id="email_id_info" class="alert alert-danger" role="alert" style="display: none;">
                            此帳號已使用過</div>
                    </div>
                    <?php if(! isset($flash)): ?>
                    <div class="form-group">
                        <label for="password">* 密碼</label>
                        <input type="password" class="form-control" id="password"  name="password">
                    </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label for="nickname">* 暱稱</label>
                        <input type="text" class="form-control" id="nickname"  name="nickname"
                               value="<?= $nickname ?>">
                    </div>
                    <?php if(! isset($flash)): ?>
                    <div class="form-group">
                        <label for="mobile">手機</label>
                        <input type="text" class="form-control" id="mobile"  name="mobile">
                    </div>
                    <div class="form-group">
                        <label for="address">地址</label>
                        <input type="text" class="form-control" id="address"  name="address">
                    </div>

                    <button type="submit" class="btn btn-default">註冊</button>
                    <?php endif; ?>
                </form>

            </div>
        </div>
    </div>


</div>

<?php include __DIR__. '/__page_foot.php'; ?>
<script>
    var email_id_info = $('#email_id_info');

    function formCheck() {
        email_id_info.hide();

        $.get('aj_check_email_id.php', {email_id:form1.email_id.value}, function(data){
            if(data != '0') {
                email_id_info.show();
            } else {
                form1.submit();
            }
        });
        
        //console.log('2342');

        return false;
    }



</script>
