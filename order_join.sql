SELECT * FROM `order_details` d JOIN `orders` o ON o.sid=d.order_sid;
SELECT * FROM `order_details` d JOIN `orders` o ON o.sid=d.order_sid WHERE o.member_sid=1;
SELECT * FROM `order_details` d JOIN `orders` o ON o.sid=d.order_sid WHERE o.sid=4;

SELECT d.*, m.nickname, m.email_id FROM `order_details` d
  JOIN `orders` o ON o.sid=d.order_sid
  JOIN `members` m ON o.member_sid=m.sid
  WHERE o.sid=4;


SELECT o.amount totalAmount, o.order_date, d.*, p.bookname
FROM `order_details` d
JOIN `orders` o ON o.sid=d.order_sid
JOIN `products` p ON p.sid=d.product_sid
WHERE o.member_sid=1
ORDER BY o.sid DESC, d.sid ASC;


-- first
SELECT *
FROM `orders`
WHERE member_sid=1
ORDER BY sid DESC;

-- second
SELECT d.*, p.bookname
FROM `order_details` d
JOIN `products` p ON p.sid=d.product_sid
WHERE order_sid IN (4,5,9)
ORDER BY d.order_sid DESC, d.sid ASC;


