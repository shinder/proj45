<?php
require __DIR__. '/__connect_db.php';
require __DIR__. '/__tools.php';
$page = 'product_list3';
$title = '產品列表';

?>
<?php include __DIR__. '/__page_head.php'; ?>
<?php include __DIR__. '/__page_navbar.php'; ?>
<div class="container">
    <div class="col-lg-3">
        <div class="col-lg-12">
            <div class="btn-group-vertical col-lg-12">
                <a href="#0" type="button" class="btn btn-default">全部商品</a>
                <a href="#1" type="button" class="btn btn-default">
                    程式設計</a>
                <a href="#2" type="button" class="btn btn-default">
                    繪圖軟體</a>
                <a href="#3" type="button" class="btn btn-default">
                    網際網路應用</a>
            </div>
        </div>
    </div>

    <div class="col-lg-9" id="p_container">

    </div>

</div>

<?php include __DIR__. '/__page_foot.php'; ?>
<script type="text/x-template" id="p_tmp">
    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="thumbnail" style="height:280px; margin:10px 0;">
            <a class="single_product" href="" target="_blank">
                <img src="imgs/small/<%= book_id %>.jpg" style="width: 100px; height: 135px;">
            </a>
            <div class="caption">
                <h5><%= bookname %></h5>
                <h5><%= author %></h5>
                <p>
                    <span class="glyphicon glyphicon-search"></span>
                    <span class="label label-info">$ <%= price %></span>
                    <select name="qn" class="qn">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                    <button class="btn btn-warning btn-sm buy_btn" data-sid="23">買</button>
                </p>
            </div>
        </div>
    </div>
</script>

<script>
    'use strict';
    var p_tmp = $('#p_tmp').text();
    var p_tmp_fnc = _.template(p_tmp);

    var getProductsData = function(cate, pageNum, search) {
        var pc = $('#p_container');
        pc.html('');
        cate = cate ? cate : 0;

        $.get('aj_product_list3.php', {cate:cate}, function(data){
            for(var i=0; i<data.data.length; i++) {
                pc.append(  p_tmp_fnc(data.data[i]) );
            }
        }, 'json');

    };
    // getProductsData();
    var hashHandler = function(){
        console.log(location.hash);
        getProductsData( parseInt(location.hash.slice(1)) );
    };
    window.addEventListener('hashchange', hashHandler);
    hashHandler();

</script>
