<?php
require __DIR__ . '/__connect_db.php';
require __DIR__ . '/__tools.php';
$page = 'buy_history';
$title = '購買歷史';


$sql = "SELECT *
FROM `orders`
WHERE member_sid= {$_SESSION['user']['sid']}
ORDER BY sid DESC;";

$o_result = $mysqli->query($sql);

while($r = $o_result->fetch_assoc()) {
    $osids[] = $r['sid'];
}
$o_result->data_seek(0);

$sql = sprintf("SELECT d.*, p.bookname
FROM `order_details` d
JOIN `products` p ON p.sid=d.product_sid
WHERE order_sid IN (%s)
ORDER BY d.order_sid DESC, d.sid ASC;", implode(',', $osids));

$d_result = $mysqli->query($sql);

?>
<?php include __DIR__ . '/__page_head.php'; ?>
<?php include __DIR__ . '/__page_navbar.php'; ?>
<div class="container">
    <div class="col-lg-12">
        <?php  while($r = $o_result->fetch_assoc()): ?>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>訂購日: <?= $r['order_date'] ?></th>
                <th>總計: <?= $r['amount'] ?></th>
            </tr>
            </thead>
            <tbody>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>書名</th>
                        <th>價錢</th>
                        <th>數量</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php while($r2 = $d_result->fetch_assoc()):
                        if( $r['sid']==$r2['order_sid']):
                        ?>
                    <tr>
                        <td><?= $r2['bookname'] ?></td>
                        <td><?= $r2['price'] ?></td>
                        <td><?= $r2['quantity'] ?></td>
                    </tr>
                    <?php
                        endif;
                    endwhile;
                    $d_result->data_seek(0);

                    ?>
                    </tbody>
                </table>

            </tbody>
        </table>
        <?php endwhile; ?>

    </div>
    <div class="col-lg-6">
        <div class="alert alert-success" role="alert">總計: <span id="totalAmount"></span></div>
    </div>
    <div class="col-lg-6">
        <?php if(isset($_SESSION['user'])): ?>
            <a class="btn btn-info" href="pay_it.php">結帳</a>
        <?php else: ?>
            <div class="alert alert-danger" role="alert">請先登入會員再結帳</div>
        <?php endif; ?>

    </div>
</div>

<?php include __DIR__ . '/__page_foot.php'; ?>
<script>

</script>
