<?php
require __DIR__. '/__connect_db.php';
require __DIR__. '/__tools.php';

$page = 'single_product';
$title = '產品';

// --- 分類選單
$cate_result = $mysqli->query("SELECT * FROM categories WHERE parent_sid=0");


$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;

if(empty($sid)){
    header('Location: product_list2.php');
    exit;
}

$result = $mysqli->query("SELECT * FROM products WHERE sid=$sid");

// echo "SELECT * FROM products WHERE sid=$sid";

$row = $result->fetch_assoc();

?>
<?php include __DIR__. '/__page_head.php'; ?>
<?php include __DIR__. '/__page_navbar.php'; ?>
<div class="container">
    <div class="col-lg-3">
        <div class="col-lg-12">
            <div class="btn-group-vertical col-lg-12">
                <a href="?cate=0" type="button"
                   class="btn btn-default">全部商品</a>
                <?php while($rowc=$cate_result->fetch_assoc()): ?>
                <a href="?cate=<?= $rowc['sid'] ?>" type="button"
                   class="btn btn-default">
                    <?= $rowc['name'] ?></a>
                <?php endwhile; ?>

            </div>
        </div>
    </div>

    <div class="col-lg-9">
        <div class="col-lg-12">

            <div class="thumbnail" style="margin:10px 0;">
                <a class="single_product" href="single_product.php?sid=<?= $row['sid'] ?>" target="_blank">
                    <img src="imgs/big/<?= $row['book_id'] ?>.png">
                </a>
                <div class="caption">
                    <h5><?= $row['bookname'] ?></h5>
                    <h5><?= $row['author'] ?></h5>
                    <p>
                        <span class="glyphicon glyphicon-search"></span>
                        <span class="label label-info">$ <?= $row['price'] ?></span>
                        <select name="qn" class="qn">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>
                        <button class="btn btn-warning btn-sm buy_btn" data-sid="23">買</button>
                    </p>
                    <p>
                        <?= $row['introduction'] ?>
                    </p>
                </div>
            </div>

        </div>

    </div>

</div>

<?php include __DIR__. '/__page_foot.php'; ?>
<script></script>
