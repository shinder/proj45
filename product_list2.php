<?php
require __DIR__. '/__connect_db.php';
require __DIR__. '/__tools.php';
$page = 'product_list2';
$title = '產品列表';
$maxRows = 8;

// --- 分類
$cate_id = isset($_GET['cate']) ? intval($_GET['cate']) : 0;
$cate_result = $mysqli->query("SELECT * FROM categories WHERE parent_sid=0");

$where = ' WHERE 1 ';
if($cate_id!=0){
    $where .= " AND category_sid=$cate_id";
}

// --- 搜尋
$search = isset($_GET['search']) ? $_GET['search'] : '';
if(! empty($search)){
    $search = $mysqli->escape_string($search);
    $where .= " AND (`author` LIKE '%{$search}%' OR `bookname` LIKE '%{$search}%'   )";
}


$result = $mysqli->query("SELECT 1 FROM products $where");
$totalRows = $result->num_rows;
$totalPages = ceil($totalRows/$maxRows);

$pageNum = isset($_GET['pageNum']) ? intval($_GET['pageNum']) : 1;

$sql = sprintf("SELECT * FROM products %s LIMIT %s, %s", $where, ($pageNum-1)*$maxRows, $maxRows);
// echo $sql;
$result = $mysqli->query($sql);

$ori = array(
    'pageNum' => $pageNum,
    'cate' => $cate_id,
    'search' => $search,
);
?>
<?php include __DIR__. '/__page_head.php'; ?>
<?php include __DIR__. '/__page_navbar.php'; ?>
<div class="container">
    <div class="col-lg-3">
        <div class="col-lg-12">
            <div class="btn-group-vertical col-lg-12">
                <a href="?cate=0" type="button"
                   class="btn btn-default <?= $cate_id==0 ? 'active' : '' ?>">全部商品</a>
                <?php while($row=$cate_result->fetch_assoc()): ?>
                <a href="?<?= bQuery($ori, ['cate' => $row['sid'], 'pageNum'=>1]) ?>" type="button"
                   class="btn btn-default <?= $cate_id==$row['sid'] ? 'active' : '' ?>">
                    <?= $row['name'] ?></a>
                <?php endwhile; ?>
<!--                <button type="button"  class="btn btn-info">2</button>-->
            </div>
        </div>
    </div>

    <div class="col-lg-9">
        <div class="col-lg-12">
            <div class="col-lg-6">
                <ul class="pagination">
                    <li><a href="?pageNum=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                    <?php for($i=$pageNum-3; $i<=$pageNum+3; $i++):
                        if($i>=1 and $i<=$totalPages):
                            $active = $i==$pageNum ? 'active' : '';
                            printf('<li class="%s"><a href="?%s">%s</a></li>',
                                $active,
                                bQuery($ori, ['pageNum' => $i]),
                                $i);

                        endif;
                    endfor; ?>
                    <li><a href="?pageNum=<?= $totalPages ?>" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                </ul>

            </div>
            <div class="col-lg-6">
                <form class="form-inline" method="get">
                    <div class="form-group">
                        <input type="hidden" name="pageNum" value="1">
                        <input type="hidden" name="cate" value="<?= $cate_id ?>">
                        <input type="text" class="form-control" value="<?= $search ?>"
                               id="search" name="search" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
            </div>
        </div>

        <?php while($row=$result->fetch_assoc()): ?>
        <div class="col-lg-3 col-sm-4 col-xs-6">
            <div class="thumbnail" style="height:280px; margin:10px 0;">
                <a class="single_product" href="single_product.php?sid=<?= $row['sid'] ?>" target="_blank">
                    <img src="imgs/small/<?= $row['book_id'] ?>.jpg" style="width: 100px; height: 135px;">
                </a>
                <div class="caption">
                    <h5><?= $row['bookname'] ?></h5>
                    <h5><?= $row['author'] ?></h5>
                    <p>
                        <span class="glyphicon glyphicon-search"></span>
                        <span class="label label-info">$ <?= $row['price'] ?></span>
                        <select name="qn" class="qn">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>
                        <button class="btn btn-warning btn-sm buy_btn" data-sid="<?= $row['sid'] ?>">買</button>
                    </p>
                </div>
            </div>
        </div>
        <?php endwhile; ?>


    </div>

</div>

<?php include __DIR__. '/__page_foot.php'; ?>
<script>
    $('.buy_btn').click(function(){
        var sid = $(this).data('sid');
//        var qn = $(this).prev().val();
        var qn = $(this).closest('.caption').find('.qn').val();
        console.log({sid:sid,qn:qn});

        $.get('add_to_cart.php', {sid:sid,qn:qn}, function(data){
            console.log(data);
            countTotal(data);

        }, 'json');
    });
</script>
