<?php
require __DIR__. '/__connect_db.php';

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;
//$book_id = isset($_GET['book_id']) ? $_GET['book_id'] : '';

$qn = isset($_GET['qn']) ? intval($_GET['qn']) : 0;


if(! isset($_SESSION['cart'])) {
    $_SESSION['cart'] = array();
}

if(! empty($sid)) {
    
    if(empty($qn)) {
        unset( $_SESSION['cart'][$sid] );
    } else {
        $_SESSION['cart'][$sid] = $qn;
    }
    
}

echo json_encode($_SESSION['cart'], JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);



