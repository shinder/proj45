<?php
require __DIR__ . '/__connect_db.php';
require __DIR__ . '/__tools.php';
$page = 'pay_it';
$title = '結帳';

if(! isset($_SESSION['user'])){
    header('Location: cart_list.php');
    exit;
}


if(! empty($_SESSION['cart'])){
    $totalAmount = 0;
    $keys = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM products WHERE sid IN (%s)", implode(',', $keys));
    $result = $mysqli->query($sql);

    $p_data = array();

    while($row=$result->fetch_assoc()) {
        $row['qn'] = $_SESSION['cart'][$row['sid']];
        $p_data[ $row['sid'] ] = $row;

        $totalAmount += $row['qn']*$row['price'];
    }


    //寫入 orders

    $sql = sprintf("INSERT INTO `orders`(`member_sid`, `amount`, `order_date`) VALUES 
(%s, %s, NOW())",
        intval($_SESSION['user']['sid']),
        $totalAmount
        );

    $mysqli->query($sql);
    $o_sid = $mysqli->insert_id;

    foreach($p_data as $k=>$v ) {
        $sql = sprintf("INSERT INTO `order_details`(`order_sid`, `product_sid`, `price`, `quantity`) VALUES
(%s, %s, %s, %s)",
            $o_sid,
            $v['sid'],
            $v['price'],
            $v['qn']
            );
        $mysqli->query($sql);
    }

    unset($_SESSION['cart']);
}







?>
<?php include __DIR__ . '/__page_head.php'; ?>
<?php include __DIR__ . '/__page_navbar.php'; ?>
<div class="container">
    <div class="col-lg-12">
        <?php if(! empty( $p_data )): ?>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>

                <th>書名</th>
                <th>作者</th>
                <th>封面</th>
                <th>單價</th>
                <th>數量</th>
                <th>小計</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($keys as $v): ?>
            <tr data-sid="<?= $v ?>">

                <td><?=  $p_data[$v]['bookname'] ?></td>
                <td><?=  $p_data[$v]['author'] ?></td>
                <td><img src="imgs/small/<?=  $p_data[$v]['book_id'] ?>.jpg" alt=""></td>
                <td data-price=""><?=  $p_data[$v]['price'] ?></td>
                <td><?=  $p_data[$v]['qn'] ?></td>
                <td class="subTotal"><?=  $p_data[$v]['price']*$p_data[$v]['qn'] ?></td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php endif; ?>

    </div>
    <div class="col-lg-6">
        <div class="alert alert-success" role="alert">總計: <span id="totalAmount">
                <?= $totalAmount ?>
            </span></div>
    </div>
    <div class="col-lg-6">
        <div class="alert alert-danger" role="alert">感蝦購買</div>
    </div>
</div>

<?php include __DIR__ . '/__page_foot.php'; ?>
<script>


</script>
