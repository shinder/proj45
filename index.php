<?php
require __DIR__. '/__connect_db.php';
?>

<?php include __DIR__. '/__page_head.php'; ?>

<?php include __DIR__. '/__page_navbar.php'; ?>

<div class="container">
    <div class="col-lg-6 col-sm-6 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">Panel heading without title</div>
            <div class="panel-body">
                Panel content
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-sm-6 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">Panel heading without title</div>
            <div class="panel-body">
                Panel content
            </div>
        </div>
    </div>

</div>

<?php include __DIR__. '/__page_foot.php'; ?>
<script></script>
