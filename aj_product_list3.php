<?php
require __DIR__. '/__connect_db.php';
require __DIR__. '/__tools.php';
$page = 'product_list2';
$title = '產品列表';
$maxRows = 200;

// --- 分類
$cate_id = isset($_GET['cate']) ? intval($_GET['cate']) : 0;
$cate_result = $mysqli->query("SELECT * FROM categories WHERE parent_sid=0");

$where = ' WHERE 1 ';
if($cate_id!=0){
    $where .= " AND category_sid=$cate_id";
}

// --- 搜尋
$search = isset($_GET['search']) ? $_GET['search'] : '';
if(! empty($search)){
    $search = $mysqli->escape_string($search);
    $where .= " AND (`author` LIKE '%{$search}%' OR `bookname` LIKE '%{$search}%'   )";
}


$result = $mysqli->query("SELECT 1 FROM products $where");
$totalRows = $result->num_rows;
$totalPages = ceil($totalRows/$maxRows);

$pageNum = isset($_GET['pageNum']) ? intval($_GET['pageNum']) : 1;

$sql = sprintf("SELECT * FROM products %s LIMIT %s, %s", $where, ($pageNum-1)*$maxRows, $maxRows);
// echo $sql;
$result = $mysqli->query($sql);

$mydata = array(
    'totalPages' => $totalPages,
    'maxRows' => $maxRows,
    'pageNum' => $pageNum,
    'cate' => $cate_id,
    'search' => $search,
    'data' => array(),
);

while($row = $result->fetch_assoc()) {
    $mydata['data'][] = $row;
}

echo json_encode($mydata, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);