<nav class="navbar navbar-default">
    <div class="container">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./">Shinder</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?= $page=='product_list' ? 'active' : '' ?>"><a href="product_list.php">產品</a></li>
                    <li class="<?= $page=='product_list2' ? 'active' : '' ?>"><a href="product_list2.php">產品列表2*</a></li>
                    <li class="<?= $page=='product_list3' ? 'active' : '' ?>"><a href="product_list3.php">產品列表3</a></li>
                    <li class="<?= $page=='cart_list' ? 'active' : '' ?>"><a href="cart_list.php">購物車 <span class="badge"></span></a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <?php if(isset($_SESSION['user'])): ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                               aria-expanded="false"><?= $_SESSION['user']['nickname'] ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="member_data.php">修改會員資料</a></li>
                                <li><a href="buy_history.php">購買歷史</a></li>
                                <li><a href="logout.php">登出</a></li>

                            </ul>
                        </li>

                    <?php else: ?>
                        <li class="<?= $page=='login' ? 'active' : '' ?>"><a href="login.php">登入</a></li>
                        <li class="<?= $page=='register' ? 'active' : '' ?>"><a href="register.php">註冊</a></li>
                    <?php endif; ?>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </div>
</nav>